import re

TURKISH_NONCAPITALS = "[abc\u00E7defg\u011Fh\u0131ijklmno\u00F6prs\u015Ftu\u00FCvyzwqx]"
TURKISH_CAPITALS = "[ABC\u00C7DEFG\u011EHI\u0130JKLMNO\u00D6PRS\u015ETU\u00DCVYZWQX]"
TURKISH_ALL = "[ABC\u00C7DEFG\u011EHI\u0130JKLMNO\u00D6PRS\u015ETU\u00DCVYZWQXabc\u00E7defg\u011Fh\u0131ijklmno\u00F6prs\u015Ftu\u00FCvyzwqx]"

def tokenize(text):
    text = text.replace("...", "")
    text = text.replace(".", "")
    tokens = re.findall(TURKISH_ALL+"+|\d*\.?\d*", text, re.U | re.I)
    for token in tokens:
        if not re.match(TURKISH_ALL+"|[0-9]", token, re.U | re.I):
            del tokens[tokens.index(token)]
    
    for token in tokens:
        if token == "":
            del tokens[tokens.index(token)]        
    
    return tokens

class Field(object):
    def __init__(self, name, expressions, optional=False):
        if type(name) != str: raise TypeError
        if type(optional) != bool: raise TypeError
        if type(expressions) != list: raise TypeError
        
        self.name = name
        self.optional = optional
        self.expressions = expressions
    
    def match(self, text):
        if type(text) != str: raise TypeError
        match = False
        
        for expr in self.expressions:
            if re.match(expr, text, re.U | re.I):
                match = True
                break
        
        return match
            

class Matcher(object):
    def __init__(self):
        self._fields = []
        self._results = None
    
    def add_field(self, field, index=None):
        if isinstance(field, Field) != True: raise TypeError
        if type(index) != int and index != None: raise TypeError
        
        for field_ in self._fields:
            if field.name == field_.name:
                raise Exception
        
        if index == None:
            self._fields.append(field)
        else:
            self._fields.insert(index, field)
    
    def remove_field(self, name):
        if type(name) != str: raise TypeError
        for field in self._fields:
            if field.name == name:
                self._fields[self._fields.index(field)]
                break
    
    def change_field_index(self, name, index):
        found = None
        for field in self._fields:
            if (field.name == name):
                found = field
        
        if found == None: raise Exception
        
        self.remove_expression(name)
        self.add_expression(found, index)
    
    def get_field_length(self):
        minl = 0
        maxl = 0
        
        for field in self._fields:
            if field.optional == True:
                maxl+=1
                minl+=1
            else:
                minl+=1
        
        return (minl, maxl)
    
    def search(self, text):
        if type(text) != str: raise TypeError
        
        results = []
        tokens = tokenize(text)
        
        cmode = False
        catch = []
        fx = 0
        tx = 0
        
        while tx < len(tokens):
            try: token = tokens[tx]
            except IndexError: break
            
            try: self._fields[fx]
            except IndexError:
                # Eğer alan indisi hata veriyorsa... sıfırla ve sonuçları ekle.
                results.append(catch)
                catch = []
                cmode = False
                fx = 0
            
            if cmode == True:
                # Eğer cmode açıksa...
                if self._fields[fx].match(token) == True:
                    # Eğer ilgili alan uyuşuyorsa... ekle ve sıradaki alana git.
                    catch.append(token)
                    fx += 1
                else:
                    # Eğer ilgili alan uyuşmuyorsa...
                    if self._fields[fx].optional == True:
                        # Eğer ilgili alan seçimlikse... geri adım at ve sıradaki alana git.
                        fx+=1
                        tx-=1
                    else:
                        # Eğer ilgili alan zorunluysa... sıfırla.
                        cmode = False
                        catch = []
                        fx = 0
                        
            
            if self._fields[0].match(token) == True:
                # İlk alan uyuşuyorsa...
                cmode = True
                catch.append(token)
                fx+=1
            
            tx += 1
            
        return results