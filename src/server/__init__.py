from flask import Flask, jsonify, request
from corpy import Field, Matcher, TURKISH_ALL

__version__ = "0.1a"
__appname__ = "recipe-parser"
__author__ = "Eray Erdin"

# Parser
quantity = Field("QUANTITY", ["bir.+", "[0-9](\.[0-9]+)?", "iki", "üç", "yarım", "çeyrek"])
quantity2 = Field("QUANTITY2", ["buçuk"], True)
scale = Field("SCALE", ["bardak", "tabak", "çanak", "ölçek", "kepçe", "kaşık", "avuç", "tane", "adet", "çay", "su"])
scale2 = Field("SCALE2", ["dolusu", "bardağı"], True)
comment = Field("COMMENT", ["taze", "bayat", "yeşil", "kırmızı", TURKISH_ALL+"+m[\u0131iu\u00FC]\u015F"], True)
ingredient = Field("INGREDIENT", [TURKISH_ALL+"+"])

matcher = Matcher()
matcher.add_field(quantity)
matcher.add_field(quantity2)
matcher.add_field(scale)
matcher.add_field(scale2)
matcher.add_field(comment)
matcher.add_field(ingredient)

# Server
app = Flask(__name__)

map = {
       "root" : "/",
       "app_root" : "/api/"+__version__,
       
       # Functions
       "parse" : "/api/"+__version__+"/parser"
       }

def error_generator(code, message):
    return {"code" : str(code), "message" : str(message)}

@app.route(map["app_root"])
def index():
    return jsonify({"author" : __author__, "appname" : __appname__, "version":__version__})

@app.route(map["parse"], methods=["POST"])
def parse():
    encoding = "UTF-8"
    text = request.get_data("text", as_text=True)
    result = matcher.search(text)
    return jsonify({"text" : text, "results" : result})